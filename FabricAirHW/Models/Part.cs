﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FabricAirHW
{
    public class Part : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public int Quantity { get; set; }
        public string Name { get; set; }
        public Fabric Fabric { get; set; }
        public Color Color { get; set; }
        public Suspension Suspension { get; set; }
        public int QuoteId { get; set; }
    }

    public enum Fabric
    {
        Lite,
        Poly,
        Glass,
        Combi
    }

    public enum Color
    {
        Blue,
        Black,
        Red,
        White,
        Yellow,
        Green,
        Grey
    }

    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum Suspension
    {
        [Description("Type-1")]
        Type1,
        [Description("Type-2")]
        Type2,
        [Description("Type-3")]
        Type3
    }
}
