﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FabricAirHW
{
    public class Quote : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public int Quantity { get; set; }
        public DateTime CreationDate { get; set; } = DateTime.UtcNow;
        public ICollection<Part> Parts { get; set; } = new List<Part>();
    }
}
