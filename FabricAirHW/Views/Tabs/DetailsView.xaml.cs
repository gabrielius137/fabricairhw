﻿using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Input;

namespace FabricAirHW
{
    /// <summary>
    /// Interaction logic for DetailsTabControl.xaml
    /// </summary>
    public partial class DetailsView : UserControl
    {
        private Regex _pattern = new Regex(@"^0$|^[1-9]{1}\d*$");

        public DetailsView()
        {
            InitializeComponent();
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!_pattern.IsMatch(e.Text))
            {
                e.Handled = true;
            }
        }
    }
}
