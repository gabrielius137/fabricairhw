﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FabricAirHW
{
    /// <summary>
    /// Interaction logic for QuotesTabControl.xaml
    /// </summary>
    public partial class QuotesView : UserControl
    {
        private Regex _pattern = new Regex(@"^0$|^[1-9]{1}\d*$");
        public QuotesView()
        {
            InitializeComponent();
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!_pattern.IsMatch(e.Text))
            {
                e.Handled = true;
            }
        }
    }
}
