﻿using System;
using System.Linq;

namespace FabricAirHW
{
    public static class Condition
    {
        private readonly static bool[,] _conditions;
        private readonly static int _rowCount;
        private readonly static int _columnCount;

        static Condition()
        {
            _conditions = new bool[,]
            {
                { true, true, false, false },
                { false, true, true, false },
                { true, true, true, true },
                { true, true, true, true },
                { true, true, true, true },
                { false, true, false, false },
                { false, true, true, true }
            };

            if ((_rowCount = _conditions.GetLength(1)) != Enum.GetValues(typeof(Fabric)).Length)
                throw new ArgumentOutOfRangeException(message: $"Check that conditions are matched with {typeof(Fabric)} enum values", paramName: nameof(_rowCount));

            if ((_columnCount = _conditions.GetLength(0)) != Enum.GetValues(typeof(Color)).Length)
                throw new ArgumentOutOfRangeException(message: $"Check that conditions are matched with {typeof(Color)} enum values", paramName: nameof(_columnCount));
        }

        public static bool[] GetColorConditions(Fabric fabric)
        {
            var col = (int)fabric;
            return Enumerable.Range(0, _rowCount)
                .Select(x => _conditions[x, col])
                .ToArray();
        }

        public static bool[] GetFabricConditions(Color color)
        {
            var row = (int)color;
            return Enumerable.Range(0, _columnCount)
                .Select(x => _conditions[row, x])
                .ToArray();
        }

        public static bool GetCondition(Color color, Fabric fabric)
        {
            var row = (int)color;
            var col = (int)fabric;
            return _conditions[row, col];
        }
    }
}
