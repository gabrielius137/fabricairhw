﻿using System;
using System.Windows.Input;

namespace FabricAirHW
{
    public class SyncCommand : ICommand
    {
        private readonly Action<object> _execute;
        private readonly Predicate<object> _canExecute;

        public SyncCommand(Action<object> execute, Predicate<object> canExecute)
        {
            _execute = execute ?? throw new ArgumentNullException(message: "Provided argument is null", 
                                                                  paramName: nameof(Action<object>));
            _canExecute = canExecute ?? throw new ArgumentNullException(message: "Provided argument is null",
                                                                        paramName: nameof(Predicate<object>));
        }

        public SyncCommand(Action<object> execute) : this(execute, (param) => true ) { }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter) => _canExecute(parameter);

        public void Execute(object parameter) => _execute(parameter);
    }
}
