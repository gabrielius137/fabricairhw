﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FabricAirHW
{
    public class QuoteRepository : BaseRepository<Quote>
    {
        public QuoteRepository(FabricAirContext context) : base(context)
        {
        }
    }
}
