﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FabricAirHW
{
    public class UnitOfWork : IDisposable
    {
        private readonly FabricAirContext _context = new FabricAirContext();
        private PartRepository _partRepository;
        private QuoteRepository _quoteRepository;

        public PartRepository PartRepository => _partRepository
                    ?? (_partRepository = new PartRepository(_context));

        public QuoteRepository QuoteRepository => _quoteRepository
                    ?? (_quoteRepository = new QuoteRepository(_context));

        public int Save() => _context.SaveChanges();

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            disposed = true;
        }

        public bool AnyPendingChanges => _context.ChangeTracker.HasChanges();

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
