﻿using System.Data.Entity;

namespace FabricAirHW
{
    public class FabricAirContext : DbContext
    {
        public FabricAirContext() : base("fabric_air")
        {

        }

        public DbSet<Part> Part { get; set; }
        public DbSet<Quote> Quote { get; set; }
    }
}
