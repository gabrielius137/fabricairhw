﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FabricAirHW
{
    public class UtcToLocalDateTimeConverter : BaseValueConverter<UtcToLocalDateTimeConverter>
    {
        /// <summary>
        /// Converts utc to local time
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is DateTime)
            {
                return ((DateTime)value).ToLocalTime();
            }
            else
            {
                throw new ArgumentException(message: $"Provided value is not {nameof(DateTime)}");
            }
        }

        /// <summary>
        /// Converts local to utc time
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is DateTime)
            {
                return ((DateTime)value).ToUniversalTime();
            }
            else
            {
                throw new ArgumentException(message: $"Provided value is not {nameof(DateTime)}");
            }
        }
    }
}
