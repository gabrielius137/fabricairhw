﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;

namespace FabricAirHW
{
    public class EnumsToStateConverter : BaseMultiValueConverter<EnumsToStateConverter>
    {
        public override object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Contains(DependencyProperty.UnsetValue))
                return Application.Current.Resources["BackgroundErrorBrush"];
            var color = (Color)values[0];
            var fabric = (Fabric)values[1];
            var state = Condition.GetCondition(color, fabric);
            if (state)
                return Application.Current.Resources["BackgroundSuccessBrush"];
            else
                return Application.Current.Resources["BackgroundErrorBrush"];
        }

        public override object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
