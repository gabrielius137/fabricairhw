﻿using Prism.Events;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace FabricAirHW
{
    public abstract class BaseViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// The event is fired when any child property changes it's value
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Call this to fire a <see cref="PropertyChanged"/> event
        /// </summary>
        /// <param name="propertyName"></param>
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Indicates if set and storage values are different and if they are fires <see cref="OnPropertyChanged(string)"/> event
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storage"></param>
        /// <param name="value"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        protected virtual bool SetProperty<T>(ref T storage, T value, [CallerMemberName]string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(storage, value))
                return false;
            storage = value;
            OnPropertyChanged(propertyName);
            return true;
        }

        // Try to figure out how to pass property value as ref would be great
        //protected virtual bool SetProperty<T>(ref T storage, T value, [CallerMemberName]string propertyName = null)
        //{
        //    if (EqualityComparer<T>.Default.Equals(storage, value))
        //        return false;
        //    storage = value;
        //    OnPropertyChanged(propertyName);
        //    return true;
        //}
    }
}