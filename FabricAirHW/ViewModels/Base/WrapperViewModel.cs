﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace FabricAirHW
{
    public abstract class WrapperViewModel<TModel> : BaseViewModel where TModel : class, new()
    {
        private TModel _model;
        private bool _isSelected;

        protected WrapperViewModel(TModel model)
        {
            _model = model ?? throw new ArgumentNullException(
                                    message: "Model can't be null",
                                    paramName: nameof(TModel));
        }

        protected virtual TModel Model
        {
            get { return _model; }
            set { SetProperty(ref _model, value); }
        }

        public virtual bool IsSelected
        {
            get { return _isSelected; }
            set { SetProperty(ref _isSelected, value); }
        }

        public abstract bool IsValid { get; }
    }
}
