﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FabricAirHW
{
    public class PartViewModel : WrapperViewModel<Part>
    {
        public PartViewModel(Part model) : base(model)
        {

        }

        public int Number => Model.Id;

        public int Quantity
        {
            get { return Model.Quantity; }
            set
            {
                if (value != Model.Quantity)
                {
                    Model.Quantity = value;
                    OnPropertyChanged();
                }
            }
        }

        public string Name
        {
            get { return Model.Name; }
            set
            {
                if (value != Model.Name)
                {
                    Model.Name = value;
                    OnPropertyChanged();
                }
            }
        }

        public Fabric Fabric
        {
            get { return Model.Fabric; }
            set
            {
                if (value != Model.Fabric)
                {
                    Model.Fabric = value;
                    OnPropertyChanged();
                    OnPropertyChanged(nameof(Color));
                    OnPropertyChanged(nameof(IsValid));
                }
            }
        }

        public Color Color
        {
            get { return Model.Color; }
            set
            {
                if (value != Model.Color)
                {
                    Model.Color = value;
                    OnPropertyChanged();
                    OnPropertyChanged(nameof(Fabric));
                    OnPropertyChanged(nameof(IsValid));
                }
            }
        }

        public Suspension Suspension
        {
            get { return Model.Suspension; }
            set
            {
                if (value != Model.Suspension)
                {
                    Model.Suspension = value;
                    OnPropertyChanged();
                }
            }
        }

        public override bool IsValid => Condition.GetCondition(Color, Fabric);
    }
}
