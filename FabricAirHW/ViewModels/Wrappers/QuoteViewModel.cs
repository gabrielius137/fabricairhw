﻿using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace FabricAirHW
{
    public class QuoteViewModel : WrapperViewModel<Quote>
    {
        private ObservableCollection<PartViewModel> _parts;

        public QuoteViewModel(Quote model) : base(model)
        {
            Parts = new ObservableCollection<PartViewModel>(Model.Parts.Select(x => new PartViewModel(x)));
        }

        public int Number => Model.Id;

        public int Quantity
        {
            get { return Model.Quantity; }
            set
            {
                if (Model.Quantity != value)
                {
                    Model.Quantity = value;
                    OnPropertyChanged();
                }
            }
        }

        public DateTime CreationDate => Model.CreationDate;

        public ObservableCollection<PartViewModel> Parts
        {
            get { return _parts; }
            set { SetProperty(ref _parts, value); }
        }

        public override bool IsValid => !Parts.Any(x => !x.IsValid);
    }
}
