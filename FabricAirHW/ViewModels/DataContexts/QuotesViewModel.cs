﻿using Prism.Events;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace FabricAirHW
{
    public class QuotesViewModel : BaseViewModel, ITabViewModel
    {
        private ObservableCollection<QuoteViewModel> _items;
        private IEventAggregator _eventAggregator;
        private UnitOfWork _dataUnit;

        public QuotesViewModel(IEventAggregator eventAggregator, UnitOfWork dataUnit)
        {
            _eventAggregator = eventAggregator;
            _dataUnit = dataUnit;
            #region test data
            //var data = new List<QuoteModel>()
            //{
            //    new QuoteModel()
            //    {
            //        Id = 0,
            //        Quantity = 5,
            //        Parts = new List<PartModel>()
            //        {
            //            new PartModel()
            //            {
            //                Id = 1,
            //                Quantity = 35,
            //                Name = "Part 1-1",
            //                Fabric = Fabric.Poly,
            //                Color = Color.Black,
            //                Suspension = Suspension.Type1
            //            },
            //            new PartModel()
            //            {
            //                Id = 1,
            //                Quantity = 58,
            //                Name = "Part 1-2",
            //                Fabric = Fabric.Lite,
            //                Color = Color.Grey,
            //                Suspension = Suspension.Type2
            //            },
            //            new PartModel()
            //            {
            //                Id = 1,
            //                Quantity = 31,
            //                Name = "Part 1-3",
            //                Fabric = Fabric.Glass,
            //                Color = Color.Red,
            //                Suspension = Suspension.Type3
            //            }
            //        }
            //    },
            //    new QuoteModel()
            //    {
            //        Id = 4,
            //        Quantity = 2,
            //        Parts = new List<PartModel>()
            //        {
            //            new PartModel()
            //            {
            //                Id = 1,
            //                Quantity = 14,
            //                Name = "Part 2-1",
            //                Fabric = Fabric.Poly,
            //                Color = Color.Blue,
            //                Suspension = Suspension.Type1
            //            },
            //            new PartModel()
            //            {
            //                Id = 1,
            //                Quantity = 73,
            //                Name = "Part 2-2",
            //                Fabric = Fabric.Combi,
            //                Color = Color.Grey,
            //                Suspension = Suspension.Type2
            //            }
            //        }
            //    },
            //    new QuoteModel()
            //    {
            //        Id = 6,
            //        Quantity = 6
            //    }
            //};
            #endregion
             var data = _dataUnit.QuoteRepository.GetAll();
            var collection = data.Select(x => new QuoteViewModel(x));
            Items = new ObservableCollection<QuoteViewModel>(collection);
            AddQuote = new SyncCommand(AddQuoteExecute);
            DeleteQuotes = new SyncCommand(DeleteQuotesExecute, DeleteQuotesCanExecute);
            Save = new SyncCommand(SaveExecute, SaveCanExecute);
        }

        public ObservableCollection<QuoteViewModel> Items
        {
            get { return _items; }
            set { SetProperty(ref _items, value); }
        }

        public QuoteViewModel SelectedQuote
        {
            set { _eventAggregator.GetEvent<QuoteSelectedEvent>().Publish(value); }
        }

        public string Title => "Quotes";

        public bool IsEnabled => true;

        public ICommand AddQuote { get; }

        private void AddQuoteExecute(object arg)
        {
            var model = new Quote()
            {
                Id = Items.Any() ? Items.Max(x => x.Number) + 1 : 0
            };
            _dataUnit.QuoteRepository.Insert(model);
            Items.Add(new QuoteViewModel(model));
        }

        public ICommand DeleteQuotes { get; }

        private void DeleteQuotesExecute(object arg)
        {
            var selectedItems = Items.Where(x => x.IsSelected).ToArray();
            if (MessageBox.Show($"Are you sure you want to delete selected entry/-ies ({selectedItems.Length + selectedItems.Sum(x => x.Parts.Count)}) ?", "Message", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                foreach (var item in selectedItems)
                {
                    Items.Remove(item);
                    _dataUnit.QuoteRepository.Delete(item.Number);
                }
            }
        }

        private bool DeleteQuotesCanExecute(object obj) => Items.Any(x => x.IsSelected);

        public ICommand Save { get; }

        private void SaveExecute(object arg)
        {
            var invalidItems = Items.Where(x => !x.IsValid).ToArray();
            if (invalidItems.Any())
            {
                MessageBox.Show($"Invalid entries have been detected, they have the following numbers: {string.Join(", ", invalidItems.Select(x => x.Number))}", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                MessageBox.Show($"In total {_dataUnit.Save()} entries have been updated/inserted/deleted", "Status", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private bool SaveCanExecute(object arg) => _dataUnit.AnyPendingChanges;
    }
}
