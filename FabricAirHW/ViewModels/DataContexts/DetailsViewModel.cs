﻿using Prism.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace FabricAirHW
{
    public class DetailsViewModel : BaseViewModel, ITabViewModel
    {
        private QuoteViewModel _selectedQuote;
        private IEventAggregator _eventAggregator;
        private UnitOfWork _dataUnit;

        public DetailsViewModel(IEventAggregator eventAggregator, UnitOfWork dataUnit)
        {
            _eventAggregator = eventAggregator;
            _eventAggregator.GetEvent<QuoteSelectedEvent>().Subscribe(OnSelectedQuoteChanged);
            _dataUnit = dataUnit;
            AddDetail = new SyncCommand(AddDetailExecute);
            DeleteDetail = new SyncCommand(DeleteDetailExecute, DeleteQuotesCanExecute);
        }

        public string Title => "Details";

        public bool IsEnabled => SelectedQuote != null;

        private QuoteViewModel SelectedQuote
        {
            get { return _selectedQuote; }
            set
            {
                if (_selectedQuote != value)
                {
                    _selectedQuote = value;
                    OnPropertyChanged(nameof(IsEnabled));
                    OnPropertyChanged(nameof(Items));
                }
            }
        }

        public ObservableCollection<PartViewModel> Items => SelectedQuote.Parts;

        private void OnSelectedQuoteChanged(QuoteViewModel arg)
        {
            SelectedQuote = arg;
        }

        public ICommand AddDetail { get; }

        private void AddDetailExecute(object obj)
        {
            var model = new Part()
            {
                Id = Items.Any() ? Items.Max(x => x.Number) + 1 : 1,
                QuoteId = SelectedQuote.Number
            };
            _dataUnit.PartRepository.Insert(model);
            Items.Add(new PartViewModel(model));
        }

        public ICommand DeleteDetail { get; }

        private void DeleteDetailExecute(object obj)
        {
            var selectedItems = Items.Where(x => x.IsSelected).ToArray();
            if (MessageBox.Show($"Are you sure you want to delete selected entry/-ies ({selectedItems.Length}) ?", "Message", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                foreach (var item in selectedItems)
                {
                    Items.Remove(item);
                    _dataUnit.PartRepository.Delete(item.Number);
                }
            }
        }

        private bool DeleteQuotesCanExecute(object obj) => Items.Any(x => x.IsSelected);
    }
}
