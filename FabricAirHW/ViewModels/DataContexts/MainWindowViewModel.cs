﻿using Prism.Events;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FabricAirHW
{
    public class MainWindowViewModel : BaseViewModel
    {
        private QuoteViewModel _selectedQuote;
        private ObservableCollection<ITabViewModel> _tabs;

        public MainWindowViewModel()
        {
            var eventAggregator = new EventAggregator();
            var dataUnit = new UnitOfWork();
            Tabs = new ObservableCollection<ITabViewModel>()
            {
                new QuotesViewModel(eventAggregator, dataUnit),
                new DetailsViewModel(eventAggregator, dataUnit)
            };
        }

        public ObservableCollection<ITabViewModel> Tabs
        {
            get { return _tabs; }
            set { SetProperty(ref _tabs, value); }
        }

        public QuoteViewModel SelectedQuote
        {
            get { return _selectedQuote; }
            set { SetProperty(ref _selectedQuote, value); }
        }

    }
}
