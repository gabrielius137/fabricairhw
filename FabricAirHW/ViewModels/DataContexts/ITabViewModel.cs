﻿namespace FabricAirHW
{
    public interface ITabViewModel
    {
        string Title { get; }
        bool IsEnabled { get; }
    }
}
